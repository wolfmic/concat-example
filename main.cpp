/*
**	Filename:	main.cpp
**	Author:		wolf
**	Email:		wolfheimrick@gir.ovh
**	Created:	2018-06-05 17:33:39
*/

#include <iostream>
#include "my_strlib.h"

int main(int ac, char **av) {
    if (ac < 2) {
        return -1;
    }
    char *str;
    str = new char[my_strlen(av[1]) + my_strlen(av[2]) + 1];
    my_concat(&str, av[1], av[2]);
    std::cout << str << std::endl;
    delete[] str;
    return 0;
}