/*
**	Filename:	my_strlib.c
**	Author:		wolf
**	Email:		wolfheimrick@gir.ovh
**	Created:	2018-06-07 11:11:09
*/

#include "my_strlib.h"

int my_strlen(const char *str) {
    int size = 0;

    while (str[size++] != '\0');
    return size - 1;
}

void my_concat(char **destination, const char* str1, const char* str2) {
    char *ptr = *destination;

    const int len1 = my_strlen(str1);
    const int len2 = my_strlen(str2);

    for (int i = 0; i < len1; i++) {
        ptr[i] = str1[i];
    }
    for (int i = 0; i < len1+len2; i++) {
        ptr[i+len1] = str2[i];
    }
}